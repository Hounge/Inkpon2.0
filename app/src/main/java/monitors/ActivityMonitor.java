package monitors;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.Browser;

import services.BackgroundService;
import com.inkpon.app.RunningAppActivity;


import java.util.List;

/**
 * Created by hounge on 9/25/2014.
 */

public class ActivityMonitor {


    public RunningAppActivity MyActivity;


    public ActivityMonitor(RunningAppActivity activity) {

        MyActivity = activity;



    }

    public void ShowStatus(String status){

        MyActivity.StatusTextView.setText(status);

    }

    public void ShowResults(String result,String type){

        if(type==""){

            MyActivity.ResultText.setText(result);
        }else if(type=="append"){

            MyActivity.ResultText.append(result);
        }


    }



    public void StartService(){

        //Start the service.
        ShowStatus("Starting service...");
        MyActivity.startService(new Intent(MyActivity, BackgroundService.class));

    }

    public void StopService(){

        MyActivity.stopService(new Intent(MyActivity, BackgroundService.class));

    }


    public void GetRunningProcesses(){
        ShowStatus("Running Processes...");
        ShowResults("Test results are coming...\n\n", "");
        //initialize Activity Manager
        ActivityManager activityManager = (ActivityManager)MyActivity.getSystemService(Context.ACTIVITY_SERVICE);

        //get list of running apps and add to a list object
        List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfo = activityManager.getRunningAppProcesses();

        for (int i = 0; i < runningAppProcessInfo.size(); i++) {

            //print to text field.

            String Item = runningAppProcessInfo.get(i).processName + "\n\n";

            ShowResults(Item, "append");

        }

    }

    public void GetRunningTasks(){
        this.ShowStatus("Running Tasks...");
        this.ShowResults("Test results are coming...\n\n", "");

        //initialize Activity Manager
        ActivityManager activityManager = (ActivityManager)MyActivity.getSystemService(Context.ACTIVITY_SERVICE);

        //Add list of running apps to a list object
        try {
            List<ActivityManager.RunningTaskInfo> getRunningTaskInfo = activityManager.getRunningTasks(50);


            for (int i = 0; i < getRunningTaskInfo.size(); i++) {

                //print to text field.

                String Item = getRunningTaskInfo.get(i).baseActivity + "\n\n";

                this.ShowResults(Item,"append");


            }




        } catch (SecurityException e) {


            this.ShowStatus(e.toString());



        }




    }

    public void GetRecentTasks(){
        ShowStatus("Recent Tasks...");
        ShowResults("Test results are coming...\n\n","");
        //initialize Activity Manager
        ActivityManager activityManager = (ActivityManager)MyActivity.getSystemService(Context.ACTIVITY_SERVICE);

        //Add list of running apps to a list object
        List<ActivityManager.RecentTaskInfo> getRecentTaskInfo = activityManager.getRecentTasks(50,MyActivity.ReturnField("RECENT_WITH_EXCLUDED"));

        for (int i = 0; i < getRecentTaskInfo.size(); i++) {

            //print to text field.

            String Item = getRecentTaskInfo.get(i).origActivity + "\n\n";

            ShowResults(Item, "append");

        }

    }

    public void GetBrowserHistory(){

        ShowStatus("Browser History...");
        ShowResults("Test results are coming...\n", "");


        String[] proj = new String[] { Browser.BookmarkColumns.TITLE, Browser.BookmarkColumns.URL };
        String sel = Browser.BookmarkColumns.BOOKMARK + " = 0"; // 0 = history, 1 = bookmark

        Cursor mCur = MyActivity.getContentResolver().query(Browser.BOOKMARKS_URI, proj, sel, null, null);


        if (mCur.moveToFirst())
        {

            @SuppressWarnings("unused")

            String title = "";


            @SuppressWarnings("unused")
            String url = "";

            if (mCur.moveToFirst() && mCur.getCount() > 0) {
                boolean cont = true;
                while (mCur.isAfterLast() == false && cont) {
                    title = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.TITLE));
                    url = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.URL));
                    // Do something with title and url

                    ShowResults(title + "\n" + url + "\n\n", "");

                    mCur.moveToNext();
                }
            }

        }
        else
        {
            ShowResults("List is empty", "");

        }

    }


    public void GetCurrentEvents() {

        ShowStatus("Current System Events...");
        ShowResults("Events coming...\n", "");

        //create new call monitor


    }

    public String GetCurrentActivity(){
        /*WARNING
        Note: this method is only intended for debugging and presenting task management user interfaces.
        This should never be used for core logic in an application, such as deciding between different behaviors
         based on the information found here. Such uses are not supported, and will likely break in the future.
         For example, if multiple applications can be actively running at the same time, assumptions made about
          the meaning of the data here for purposes of control flow will be incorrect.

         */

        //initialize Activity Manager
        ActivityManager activityManager = (ActivityManager)MyActivity.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> getRunningTaskInfo = activityManager.getRunningTasks(1);

        ComponentName componentInfo = getRunningTaskInfo.get(0).topActivity;

        String ClassName = getRunningTaskInfo.get(0).topActivity.getClassName();

        String PackageName = componentInfo.getPackageName();

        String Results = "Package Name: "+PackageName;
        Results = Results + "Class Name: "+ClassName;

        ShowResults(Results,"append");

        return Results;
    }

    public void GetBroadcastReceiver(){




    }


    public void IncomingSMS(){



    }


    public void OutgoingSMS(){



    }


    public void IncomingCalls (){



    }

    public void OutgoingCalls(){




    }

    public void DataConsumption(){



    }


}