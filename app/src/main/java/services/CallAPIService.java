package services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import services.ParseJSONService;

/**
 * Created by hounge on 9/26/14.
 */
public class CallAPIService extends Service {
    public static final String Stored_Token = "TokenFile";


    public void onCreate() {
        SharedPreferences prefs = getSharedPreferences(Stored_Token,MODE_PRIVATE);
        String Token = prefs.getString("Token", null);
        String APICALL = prefs.getString("APICall", null);
        Log.i("APICALL", APICALL); //testing to see values are correct
        Log.i("Token", Token);

        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //creates new JSON object to send with Post contains Token from login
            JSONObject parametersList = new JSONObject();
            parametersList.put("Token",Token);
            System.out.println(Token);
            //sets headers and Posts to URL
            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(APICALL);
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            httpPost.setEntity(new StringEntity(parametersList.toString(),HTTP.UTF_8));
            //gets Response from URL
            HttpResponse responseGet = client.execute(httpPost);
            //converts Response to String and store in shared preferences
            int statusCode = responseGet.getStatusLine().getStatusCode();
            String jsonResponse = EntityUtils.toString(responseGet.getEntity());
            SharedPreferences.Editor editor = getSharedPreferences(Stored_Token,MODE_PRIVATE).edit();
            editor.putString("JSONARRAY",jsonResponse);
            editor.apply();
            //get status codes from URL and log response for debug
            if (statusCode != HttpStatus.SC_OK) {
                Log.i("TAG", "Error in web request: " + statusCode);
                Log.i("TAG",jsonResponse);
            } else {
                Log.i("TAG",jsonResponse);
              }
        } catch (IOException e) {
            e.printStackTrace();
          } catch (JSONException e) {
            e.printStackTrace();
            }
        Intent i = new Intent(this, ParseJSONService.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(i);

        //kills service after complete
        stopSelf();





    }





    public IBinder onBind(Intent intent) {
        return null;
    }
}

