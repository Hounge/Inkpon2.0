package services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by hounge on 10/17/14.
 */
public class ChatService extends Service {
    public static final String Stored_Token = "TokenFile";
    private final Handler handler = new Handler();
    SharedPreferences appPref;
    SharedPreferences.Editor appPrefEditor;


    private Runnable CheckNewMessages = new Runnable()
    {
        public void run()
        {
            ChatService.this.GetNewChat();
            ChatService.this.handler.postDelayed(this, 15000); /*sets the how often this runs*/
        }
    };


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.handler.removeCallbacks(this.CheckNewMessages);
        this.handler.postDelayed(this.CheckNewMessages, 1000); /*sets the delay between checking messages*/

        return super.onStartCommand(intent, flags, startId);

    }


    public void oncreate() {

    }
        private void GetNewChat() {

            appPref = getSharedPreferences(Stored_Token, MODE_PRIVATE);
            appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
            String Token = appPref.getString("Token", null);
            String APICALL = "http://www.incpon.com/inkpon/GetActivities";

            Log.i("APICALL", APICALL); //testing to see values are correct

            try {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                JSONObject Chat = new JSONObject();
                Chat.put("Token", Token);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(APICALL);
                httppost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                httppost.setEntity(new StringEntity(Chat.toString(), HTTP.UTF_8));
                HttpResponse response = httpclient.execute(httppost);
                String jsonResponse = EntityUtils.toString(response.getEntity());
                try {
                    JSONArray jArr = new JSONArray(jsonResponse);
                    for (int i = 0; i < jArr.length(); i++) {
                        try {
                            JSONObject jsonObject = jArr.getJSONObject(i);
                            int  TxtMessage = jsonObject.getInt("TxtMessage");
                            Log.i("newmessage",String.valueOf(TxtMessage) );
                            if (TxtMessage != 0) {
                                /*this is what happens when you have a new unread message
                                 Need to add the logic so that the toast doesnt repeat every 15 seconds
                                 the server needs to know wh checked the message
                                 this can be a system notification or a system sound or both*/
                                Toast.makeText(getApplicationContext(), "You have a new Message ", Toast.LENGTH_LONG).show();
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


            @Override
    public IBinder onBind (Intent intent){
        return null;
    }
}
