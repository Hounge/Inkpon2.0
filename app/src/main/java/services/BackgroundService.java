package services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;


import java.util.Date;
import java.util.List;

/**
 * Created by hounge on 9/25/2014.
 */

public class BackgroundService extends Service {
    public static final String BROADCAST_ACTION = "Inkpon";
    private static final String TAG = "BroadcastService";
    SQLiteDatabase db;
    BroadcastReceiver MyReceiver;
    private final Handler handler = new Handler();
    Intent intent;

    private Runnable sendUpdatesToUI = new Runnable()
    {
        public void run()
        {
            BackgroundService.this.DisplayLoggingInfo();
            BackgroundService.this.handler.postDelayed(this, 15000);
        }
    };

    private void DisplayLoggingInfo()
    {
        db= openOrCreateDatabase("Mydb", MODE_PRIVATE, null);
        db.execSQL("create table if not exists mytable(txtDateTime varchar)");

        String str = GetCurrentActivity();
        this.intent.putExtra("time", new Date().toLocaleString() + "  " + str);
        String time = intent.getStringExtra("time");

        db.execSQL("insert into mytable values('"+time+"')");


        sendBroadcast(this.intent);
    }




    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.handler.removeCallbacks(this.sendUpdatesToUI);
        this.handler.postDelayed(this.sendUpdatesToUI, 1000);

        //Start the service thread.
        MyThread thethread = new MyThread(this);
        thethread.start();


        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onCreate() {
        super.onCreate();


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        this.intent = new Intent("Inkpon");
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_USER_PRESENT);

        this.MyReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {


            }
        };
        this.registerReceiver(this.MyReceiver, filter);

        //Warning it is important to not forget to unregister your receiver in the onDestroy() method!

    }





    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy(){


        stopSelf();
        this.unregisterReceiver(this.MyReceiver);
        super.onDestroy();
        handler.removeCallbacks(sendUpdatesToUI);
        Intent homeIntent= new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }



    private class MyThread extends Thread{
        //run service in it's own thread.
        Context context;

        public MyThread(Context context){

            this.context = context;
        }

        public void run(){
            //the service code that will actually run.



        }

    }






    public String GetCurrentActivity(){
            /*WARNING
            Note: this method is only intended for debugging and presenting task management user interfaces.
            This should never be used for core logic in an application, such as deciding between different behaviors
             based on the information found here. Such uses are not supported, and will likely break in the future.
             For example, if multiple applications can be actively running at the same time, assumptions made about
              the meaning of the data here for purposes of control flow will be incorrect.

             */

        //initialize Activity Manager
        ActivityManager activityManager = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> getRunningTaskInfo = activityManager.getRunningTasks(2);

        ComponentName componentInfo = getRunningTaskInfo.get(0).topActivity;

        String ClassName = getRunningTaskInfo.get(0).topActivity.getClassName();

        String PackageName = componentInfo.getPackageName();

        String Results = GetProperAppName(PackageName);
        return Results;
    }

    public String GetProperAppName(String PackageName){

        final PackageManager pm = getApplicationContext().getPackageManager();

        try {

            ApplicationInfo AppInfo = pm.getApplicationInfo(PackageName, 1);

            if(AppInfo != null){

                String AppName = (String)pm.getApplicationLabel(AppInfo);
                return AppName;

            }
            else{

                String AppName = "Inkpon: Unknown app";
                return AppName;

            }



        } catch (PackageManager.NameNotFoundException e) {

            String Error = "Inkpon Service Error: " + e.toString();
            // PopSomeToast(Error);
            return Error;


        }


    }
}