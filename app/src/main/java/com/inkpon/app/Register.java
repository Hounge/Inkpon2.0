package com.inkpon.app;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Created by hounge on 10/5/14.
 */
public class Register extends Activity {
    public static final String Stored_Token = "TokenFile";
    EditText UserName;
    EditText Email;
    EditText Password;
    EditText PasswordConfirm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registeruser);
        // Set up the registration form.
        UserName = (EditText) findViewById(R.id.UserName);
        Email = (EditText) findViewById(R.id.editTextEmail);
        Password = (EditText) findViewById(R.id.Password);
        PasswordConfirm = (EditText) findViewById(R.id.PasswordConfirm);

    }

    public void onRegister(View view) {

        // Validate the log in data
        boolean validationError = false;
        StringBuilder validationErrorMessage =
                new StringBuilder(getResources().getString(R.string.error_intro));
        if (isEmpty(UserName)) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_username));
        }
        if (isEmpty(PasswordConfirm)) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_username));
        }
        if (isEmpty(Email)) {
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_username));
        }
        if (isEmpty(Password)) {
            if (validationError) {
                validationErrorMessage.append(getResources().getString(R.string.error_join));
            }
            validationError = true;
            validationErrorMessage.append(getResources().getString(R.string.error_blank_password));
        }
        validationErrorMessage.append(getResources().getString(R.string.error_end));

        // If there is a validation error, display the error
        if (validationError) {
            Toast.makeText(Register.this, validationErrorMessage.toString(), Toast.LENGTH_LONG)
                    .show();
            return;
        }

        String Username = UserName.getText().toString();
        String email  = Email.getText().toString();

        MessageDigest encoded = null;
        try {
            encoded = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String text = Password.getText().toString();
        try {
            encoded.update(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            Log.v("ewwor", "someting wong");
            e.printStackTrace();
        }
        byte[] passhash = encoded.digest();
        BigInteger bigInt = new BigInteger(1, passhash);
        String hashed = bigInt.toString(16);

        String finalpassword = hashed.toString();
        //Log.i("email", email);
        //Log.i("password", finalpassword);

        try{
            //Create JSONObject to register with server
            JSONObject register = new JSONObject();
            register.put("UserName",Username);
            register.put("Email", email);
            register.put("Password", finalpassword);
            Resources resources=getResources();
            final String urla=String.format(resources.getString(R.string.createmember));
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //Post JSONObject to server
            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(urla);
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            httpPost.setEntity(new StringEntity(register.toString(), HTTP.UTF_8));
            //gets Response from URL
            HttpResponse responseGet = client.execute(httpPost);
            String response1 = EntityUtils.toString(responseGet.getEntity());
            Log.i("register",response1);
        } catch (Exception e) {

            e.printStackTrace();
        }
        Intent i = new Intent(Register.this, Login.class);
        startActivity(i);

    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }




}
