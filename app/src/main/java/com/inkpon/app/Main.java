package com.inkpon.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.content.SharedPreferences;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import services.ChatService;
import services.NewsService;

/**
 * Created by hounge on 9/25/2014.
 */
public class Main extends Activity {
    public static final String Stored_Token = "TokenFile";
    SharedPreferences appPref;
    SharedPreferences.Editor appPrefEditor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appPref = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        appPrefEditor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();

        setContentView(R.layout.main);
        Intent i = new Intent(this, NewsService.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(i);

        Intent ic = new Intent(this, ChatService.class);
        ic.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(ic);
    }

    public void EditProfileClick(View view) {

        Intent i = new Intent(Main.this, EditProfile.class);
        startActivity(i);
    }

    public void AccountDetailsClick(View view) {
        Resources resources=getResources();
        String RPT_user_total_minutes=String.format(resources.getString(R.string.RPT_user_total_minutes));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", RPT_user_total_minutes);
        editor.apply();
        Log.i("APICALL", RPT_user_total_minutes);
        Intent i = new Intent(Main.this, AccountDetails.class);
        startActivity(i);

    }
    public void ChatClick(View view) {
        Resources resources=getResources();
        String get_user_chat_history =String.format(resources.getString(R.string.get_user_chat_history));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", get_user_chat_history );
        editor.apply();
        Log.i("APICALL", get_user_chat_history);
        Intent i = new Intent(Main.this, Chat.class);
        startActivity(i);

    }
    public void TimetransferClick(View view) {
        Resources resources=getResources();
        String transfer_get_hours =String.format(resources.getString(R.string.transfer_get_hours));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", transfer_get_hours );
        editor.apply();
        Log.i("APICALL", transfer_get_hours);
        Intent i = new Intent(Main.this, TransactionHistory.class);
        startActivity(i);

    }
    public void DealsClick(View view) {
        Resources resources=getResources();
        String query_coupons =String.format(resources.getString(R.string.query_coupons));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", query_coupons );
        editor.apply();
        Log.i("APICALL", query_coupons);
        Intent i = new Intent(Main.this, Deals.class);
        startActivity(i);

    }
    public void SummaryClick(View view) {
        Resources resources=getResources();
        String get_user_withdrawal =String.format(resources.getString(R.string.get_user_withdrawal));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", get_user_withdrawal);
        editor.apply();
        Log.i("APICALL", get_user_withdrawal);
        Intent i = new Intent(Main.this, Summary.class);
        startActivity(i);

    }

    //Toast message for InkPon News
    public void PopToast(String msg){

        Context context = getApplicationContext();

        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, msg, duration);

        toast.show();

    }


    /**
     * ---added by cenwesi ***
     */

    @Override
    protected void onDestroy() {
        logOut();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        logOut();
        super.onBackPressed();
    }


    private void logOut() {
        String Token = appPref.getString("Token", null);
        String url = getResources().getString(R.string.logout);

        if (Token != null) {
            try {
                JSONObject Details = new JSONObject();
                Details.put("Token", Token);

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                httppost.setEntity(new StringEntity(Details.toString(), HTTP.UTF_8));
                HttpResponse response = httpclient.execute(httppost);

            /*--check http status code...*/
                int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode != HttpStatus.SC_OK) {
                    Log.d("***Logout: ", "Error login out...");
                } else {
                /* remove the token */
                    //SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
                    appPrefEditor.remove("Token");
                    appPrefEditor.apply();
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
