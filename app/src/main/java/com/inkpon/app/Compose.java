package com.inkpon.app;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by hounge on 10/5/14.
 */
public class Compose extends Activity {
    public static final String Stored_Token = "TokenFile";


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.compose);
        Resources resources=getResources();
        String add_users_chat=String.format(resources.getString(R.string.add_users_chat));
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("APICall", add_users_chat);
        editor.apply();
        Log.i("APICALL", add_users_chat);

    }

    public void BackButton(View view){
        this.finish();
    }

    public void onSend(View view) {

       new SendMessage().execute();
        this.finish();
    }

    class SendMessage extends AsyncTask<Void, Void, String> {


            SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
            String Token = prefs.getString("Token", null);
            String APICALL = prefs.getString("APICall", null);
            EditText User = (EditText) findViewById(R.id.recipient);
            EditText message = (EditText) findViewById(R.id.message);
            String UserName= User.getText().toString();
            String Str = message.getText().toString();


            @Override
            protected String doInBackground(Void... params) {
                String str = null;
                JSONObject newchat = new JSONObject();
                try {
                    newchat.put("UserName", UserName);
                    newchat.put("Str", Str);
                    newchat.put("Token", Token);

                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(APICALL);
                    httppost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                    httppost.setEntity(new StringEntity(newchat.toString(), HTTP.UTF_8));
                    HttpResponse response = httpclient.execute(httppost);
                    str = EntityUtils.toString(response.getEntity());
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("response", str);
                return str;

            }
        }

    }

