package com.inkpon.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import monitors.ActivityMonitor;
import services.BackgroundService;


public class RunningAppActivity extends Activity {
    private static final int RECENT_WITH_EXCLUDED = 1;
    private static final String TAG = "BroadcastTest";
    ActivityMonitor MyActivityMonitor;
    SQLiteDatabase db;
    public TextView ResultText;
    public TextView StatusTextView;
    Button StartServiceButton;
    Button StopServiceButton;
    private Intent intent;

   // @Override
   /* protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.runningapps);

        ResultText = (TextView) findViewById(R.id.ResultsEditText);
        StatusTextView = (TextView) findViewById(R.id.StatusTextView);
        intent = new Intent(this, BackgroundService.class);
        StartServiceButton = (Button) findViewById(R.id.ServiceStartButton);
        StopServiceButton = (Button) findViewById(R.id.StopServiceButton);
        db= openOrCreateDatabase("Mydb", MODE_PRIVATE, null);
        db.execSQL("create table if not exists mytable(txtDateTime varchar)");

        //startup activity monitor //must come after widget inits
        // all monitoring activity is run by this class
        MyActivityMonitor = new ActivityMonitor(this);

    } */
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            updateUI(intent);

        }
    };

    private void updateUI(Intent intent) {

        String time = intent.getStringExtra("time");
        Log.d(TAG, time);
    //    TextView txtDateTime = (TextView) findViewById(R.id.ResultsEditText);
    //    txtDateTime.append(" \n");
    //    txtDateTime.append(time);
        db.execSQL("insert into mytable values('"+time+"')");

    }

    //TODO
    // change the database implementation to the
    //service so service can run independently of the running app activity


    public int ReturnField(String field){


        int value = 0;
        if(field=="RECENT_WITH_EXCLUDED"){
            value = RECENT_WITH_EXCLUDED;

        }

        return value;

    }
    public void display()
    {
        String time = "";
        db.execSQL("insert into mytable values('"+time+ "')");

        //use cursor to keep all data
        //cursor can keep data of any data type
        Cursor c=db.rawQuery("select * from mytable", null);
        ResultText.setText("");
        //move cursor to first position
        c.moveToFirst();
        //fetch all data one by one
        do
        {
            //we can use c.getString(0) here
            //or we can get data using column index
            String name=c.getString(0);
            //display on text view
            ResultText.append(""+name+" \n");
            //move next position until end of the data
        }while(c.moveToNext());
    }

    @Override
    public void onResume() {

        super.onResume();
        startService(intent);
        registerReceiver(broadcastReceiver, new IntentFilter(BackgroundService.BROADCAST_ACTION));
        display();

    }




    @Override
    public void onPause() {

        super.onPause();

    }


    public void StartServiceButtonListener(View e){

        MyActivityMonitor.StartService();
    }

    public void StopServiceButtonListener(View e){


        MyActivityMonitor.StopService();

    }

}