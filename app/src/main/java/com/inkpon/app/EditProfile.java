package com.inkpon.app;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import services.GetuserinfoService;

/**
 * Created by hounge on 9/25/2014.
 */
public class EditProfile extends Activity {
    public static final String Stored_Token = "TokenFile";
    public ArrayList<String> age_group_name =null;
    public ArrayList<String> age_group_key = null;
    public ArrayList<String> ethnicity_group_name =null;
    public ArrayList<String> ethnicity_group_key = null;

//    public class MyEthnicGroup{
//        String Token;
//        int EthnicGroup;
//        MyEthnicGroup(String a, int b){
//            this.Token=a;
//            this.EthnicGroup=b;
//        }
//    }

    public String atob(String str) {
        String decodedDataUsingUTF8 = null;

        byte[] encodedhash = Base64.decode(str, Base64.NO_WRAP);
        try {
            decodedDataUsingUTF8 = new String(encodedhash, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return decodedDataUsingUTF8;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        age_group_name = new ArrayList<String>();
        age_group_key = new ArrayList<String>();
        ethnicity_group_name = new ArrayList<String>();
        ethnicity_group_key = new ArrayList<String>();


        setContentView(R.layout.edit_profile);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //get value from SharedPreferences
        SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        String GETUSERINFO = prefs.getString("GETUSERINFO", null);
        //final String Token = prefs.getString("Token", "***");


        try {
            JSONArray jArr = new JSONArray(GETUSERINFO);
            JSONObject obj = jArr.getJSONObject(0);

            //String agegroup = prefs.getString("AgeGroup", "0");
            String agegroup = obj.getString("AgeGroup");
            String ethnicitygroup = obj.getString("Ethnicity");
//System.out.println("--------=======> ethnicitygroup: "+ethnicitygroup);

            //MyEthnicGroup me = new MyEthnicGroup(Token,Integer.parseInt(ethnicitygroup));

            new DownloadAgeGroups().execute(Integer.parseInt(agegroup)); /*--start thread */
            //new DownloadEthnicityGroups().execute(me); /*--start thread */
            //new DownloadEthnicityGroups().execute(Token,ethnicitygroup);
            new DownloadEthnicityGroups().execute(ethnicitygroup.toString());

            ((EditText) findViewById(R.id.Firstname)).setText(obj.getString("FirstName"));
            ((EditText) findViewById(R.id.Lname)).setText(obj.getString("LastName"));
            ((EditText) findViewById(R.id.Email)).setText(obj.getString("Email"));
            ((EditText) findViewById(R.id.streetaddress1)).setText(obj.getString("StreetAddress1"));
            ((EditText) findViewById(R.id.streetaddress2)).setText(obj.getString("StreetAddress2"));
            ((EditText) findViewById(R.id.city)).setText(obj.getString("City"));
            ((EditText) findViewById(R.id.state)).setText(obj.getString("State"));
            ((EditText) findViewById(R.id.homenumber)).setText(obj.getString("HomePhone"));
            ((EditText) findViewById(R.id.cellnumber)).setText(obj.getString("CellPhone"));
            //((EditText) findViewById(R.id.editTextZipCode)).setText(obj.getString("Zip"));
            ((EditText) findViewById(R.id.forgot_email)).setText(obj.getString("Password"));


            /**---here i will display the users picture if it exist image */
            ImageView image = (ImageView) findViewById(R.id.imageViewUser);
            String UserImage = atob(obj.getString("picture")); /* decode the encoded image back to string...*/
            String imageDataBytes = UserImage.substring(UserImage.indexOf(",") + 1); /*-getting the binary part...notice we can test to see if it is jpg/png/bmp.... */
            InputStream stream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT)); /*-- decoding to bytes*/
            image.setImageBitmap(BitmapFactory.decodeStream(stream)); /*--display it */


            String gender = obj.getString("Gender");
            if (gender.equals("M")) {
                ((RadioButton) findViewById(R.id.radioButton)).setChecked(true);
            } else if (gender.equals("F")) {
                ((RadioButton) findViewById(R.id.radioButton2)).setChecked(true);
            } else {
                ((RadioButton) findViewById(R.id.radioButton3)).setChecked(true);
            }

            ((CheckBox) findViewById(R.id.radioButton7)).setChecked(Integer.parseInt(obj.getString("I_ArtsCrafts")) == 1);
            ((CheckBox) findViewById(R.id.radioButton8)).setChecked(Integer.parseInt(obj.getString("I_Dance")) == 1);
            ((CheckBox) findViewById(R.id.radioButton12)).setChecked(Integer.parseInt(obj.getString("I_Cycling")) == 1);
            ((CheckBox) findViewById(R.id.radioButton10)).setChecked(Integer.parseInt(obj.getString("I_ComputersIT")) == 1);
            ((CheckBox) findViewById(R.id.radioButton6)).setChecked(Integer.parseInt(obj.getString("I_Exercise")) == 1);
            ((CheckBox) findViewById(R.id.radioButton11)).setChecked(Integer.parseInt(obj.getString("I_Gaming")) == 1);
            ((CheckBox) findViewById(R.id.radioButton13)).setChecked(Integer.parseInt(obj.getString("I_HealthFitness")) == 1);
            ((CheckBox) findViewById(R.id.radioButton9)).setChecked(Integer.parseInt(obj.getString("I_Hiking")) == 1);
            ((CheckBox) findViewById(R.id.radioButton5)).setChecked(Integer.parseInt(obj.getString("I_Shopping")) == 1);
            ((CheckBox) findViewById(R.id.radioButton4)).setChecked(Integer.parseInt(obj.getString("I_Theatre")) == 1);


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void onRefresh(View v) {
        /* call our service to reload the userinfo */
        Intent i = new Intent(this, GetuserinfoService.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(i);

        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    public void onSave(View v) {

        Toast.makeText(this, "Disabled for now...", Toast.LENGTH_LONG).show();

        /*  --remmed out for now.. i need to fix the password issue
        SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        String Token = prefs.getString("Token", null);
        String Url = "http://www.incpon.com/inkpon/updatemember";
        String str = null;

        EditText firstname = (EditText) findViewById(R.id.editTextFirstname);
        EditText lastname = (EditText) findViewById(R.id.editTextLastName);
        EditText email = (EditText) findViewById(R.id.editTextEmail);
        EditText streetaddress1 = (EditText) findViewById(R.id.editTextStreetAddress1);
        EditText streetaddress2 = (EditText) findViewById(R.id.editTextStreetAddress2);
        EditText city = (EditText) findViewById(R.id.editTextCity);
        EditText state = (EditText) findViewById(R.id.editTextState);
        EditText homephone = (EditText) findViewById(R.id.editTextHomeNumber);
        EditText cellphone = (EditText) findViewById(R.id.editTextCellNumber);
        EditText password = (EditText) findViewById(R.id.editTextPassword);
        EditText zipcode = (EditText) findViewById(R.id.editTextZipCode);


        String Firstname = firstname.getText().toString();
        String Lastname = lastname.getText().toString();
        String Email = email.getText().toString();
        String StreetAddress1 = streetaddress1.getText().toString();
        String StreetAddress2 = streetaddress2.getText().toString();
        String City = city.getText().toString();
        String State = state.getText().toString();
        String Homephone = homephone.getText().toString();
        String Cellphone = cellphone.getText().toString();
        String Password = password.getText().toString();
        String ZipCode = zipcode.getText().toString();

        JSONObject objecta = new JSONObject();
        try {
            objecta.put("Token", Token);
            objecta.put("Password", Password);
            objecta.put("FirstName", Firstname);
            objecta.put("LastName", Lastname);
            objecta.put("Email", Email);
            objecta.put("StreetAddress1", StreetAddress1);
            objecta.put("StreetAddress2", StreetAddress2);
            objecta.put("City", City);
            objecta.put("State", State);
            objecta.put("HomePhone", Homephone);
            objecta.put("CellPhone", Cellphone);
            objecta.put("TMP1", "0");
            objecta.put("TMP2", "0");
            objecta.put("TMP3", "0");
            objecta.put("TMP4", "0");
            objecta.put("TMP5", "0");
            objecta.put("TMP6", "0");
            objecta.put("TMP7", "0");
            objecta.put("TMP8", "0");
            objecta.put("TMP9", "0");
            objecta.put("TMP10", "0");
            objecta.put("DateOfBirth", "01-01-1900");
            objecta.put("Ethnicity", "1");
            objecta.put("Gender", "M");
            objecta.put("Zip", ZipCode);
            objecta.put("AgeGroup", "1");
            objecta.toString();
            System.out.println(Token);
            System.out.println(objecta);


            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();

            HttpPost httppost = new HttpPost(Url);
            httppost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            httppost.setEntity(new StringEntity(objecta.toString(), HTTP.UTF_8));
            HttpResponse response = httpclient.execute(httppost);
            str = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("response", str);

        Intent i = new Intent(this, GetuserinfoService.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startService(i);

        Intent intent = getIntent();
        finish();
        startActivity(intent);
        System.out.println(str);
        */
    }


    private class DownloadEthnicityGroups extends AsyncTask<String, Void, Void> {
        public int therow = 0;
        @Override
        protected Void doInBackground(String... params) {
            therow = 0;
            therow = Integer.parseInt(params[0].toString());
            String str = null;

            try {
                String url = getResources().getString(R.string.getEthnicGroups);
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(url);
                httpget.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                HttpResponse response = httpclient.execute(httpget);
                str = EntityUtils.toString(response.getEntity());

                JSONArray new_array = new JSONArray(str);
                for (int i = 0, count = new_array.length(); i < count; i++) {
                    //try {
                        JSONObject jsonObject = new_array.getJSONObject(i);
                        ethnicity_group_name.add(jsonObject.getString("Ethnicity").toString());
                        ethnicity_group_key.add(jsonObject.getString("EthnicityID").toString());
                    //} catch (JSONException e) {
                    //    e.printStackTrace();
                    //}
                }

            } catch (IOException e) {
                e.printStackTrace();
            }catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Spinner mySpinner2 = (Spinner) findViewById(R.id.spinner2);
            mySpinner2.setAdapter(new ArrayAdapter<String>(EditProfile.this, android.R.layout.simple_spinner_dropdown_item, ethnicity_group_name));
            mySpinner2.setSelection(ethnicity_group_key.indexOf(Integer.toString(therow)));
        }
    }

    private class DownloadAgeGroups extends AsyncTask<Integer, Void, String> {
        public int therow = 0;

        @Override
        protected String doInBackground(Integer... params) {
            //return GoValidateToken(params[0].toString());
            therow = params[0];
            return getAgeGroups();
        }

        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);

            try {
                JSONArray new_array = new JSONArray(o);
                for (int i = 0, count = new_array.length(); i < count; i++) {
                    try {
                        JSONObject jsonObject = new_array.getJSONObject(i);
                        age_group_name.add(jsonObject.getString("Ages").toString());
                        age_group_key.add(jsonObject.getString("PKID").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Spinner mySpinner = (Spinner) findViewById(R.id.spinner);
                mySpinner.setAdapter(new ArrayAdapter<String>(EditProfile.this, android.R.layout.simple_spinner_dropdown_item, age_group_name));

                mySpinner.setSelection(age_group_key.indexOf(Integer.toString(therow)));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public String getAgeGroups() {
            String str = null;
            String url = getResources().getString(R.string.getAgeGroups);

            try {
                JSONObject param = new JSONObject();
                param.put("TMP1", "1");

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);

                httppost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                httppost.setEntity(new StringEntity(param.toString(), HTTP.UTF_8));
                HttpResponse response = httpclient.execute(httppost);
                str = EntityUtils.toString(response.getEntity());


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Log.i(">>> AgeGroups-->", str);
            return str;
        }

    }


    /*i will delete this later...was testing some theories out on parameters...*/
    private class DownloadEthnicityGroupsBAK_TESTING extends AsyncTask<Object,Void, String> {
        public int therow = 0;
        String Token=null;
        @Override
        protected String doInBackground(Object... params) {

//            MyEthnicGroup tmp = (MyEthnicGroup) params[0];
//            therow = (Integer) tmp.EthnicGroup;
//            Token = tmp.Token.toString();

            Token = params[0].toString();
            therow = Integer.parseInt(params[1].toString());
            return getEthnicityGroups(Token, therow);
        }


        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);

            try {
                JSONArray new_array = new JSONArray(o);
                for (int i = 0, count = new_array.length(); i < count; i++) {
                    try {
                        JSONObject jsonObject = new_array.getJSONObject(i);
                        ethnicity_group_name.add(jsonObject.getString("Ethnicity").toString());
                        ethnicity_group_key.add(jsonObject.getString("EthnicityID").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Spinner mySpinner = (Spinner) findViewById(R.id.spinner2);
                mySpinner.setAdapter(new ArrayAdapter<String>(EditProfile.this, android.R.layout.simple_spinner_dropdown_item, ethnicity_group_name));

                mySpinner.setSelection(ethnicity_group_key.indexOf(Integer.toString(therow)));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public String getEthnicityGroups(String token, int ethnicgroup) {
            String str = null;
            String url = getResources().getString(R.string.getEthnicGroups);

            try {
                JSONObject param = new JSONObject();
                param.put("Token", token);

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HttpClient httpclient = new DefaultHttpClient();
                //HttpPost httppost = new HttpPost(url);
                HttpGet httpget = new HttpGet(url);


                httpget.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                //httpget.setEntity(new StringEntity(param.toString(), HTTP.UTF_8));
                HttpResponse response = httpclient.execute(httpget);
                str = EntityUtils.toString(response.getEntity());

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i("---->>> EthnicGroups-->", str);
            return str;
        }

    }

    private class DownloadEthnicityGroupsBAK extends AsyncTask<String, Void, String> {
        int therow = 0;

        @Override
        protected String doInBackground(String... params) {
            System.out.println("---starting--->>>" + params[0].toString());

            therow = Integer.parseInt(params[0].toString());

            System.out.println("---hello--->>> ");

            return getEthnicityGroups(therow);
        }


        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);

            System.out.println("---zzzzzzzz--->>> ");

            try {
                JSONArray new_array = new JSONArray(o);
                for (int i = 0, count = new_array.length(); i < count; i++) {
                    try {
                        JSONObject jsonObject = new_array.getJSONObject(i);
                        ethnicity_group_name.add(jsonObject.getString("Ethnicity").toString());
                        ethnicity_group_key.add(jsonObject.getString("EthnicityID").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Spinner mySpinner = (Spinner) findViewById(R.id.spinner2);
                mySpinner.setAdapter(new ArrayAdapter<String>(EditProfile.this, android.R.layout.simple_spinner_dropdown_item, ethnicity_group_name));

                mySpinner.setSelection(ethnicity_group_key.indexOf(Integer.toString(therow)));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public String getEthnicityGroups(int ethnicgroup) {
            String str = null;

            System.out.println("---hereeeeeeeee----");

            try {

                String url = getResources().getString(R.string.getEthnicGroups);

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(url);

                httpget.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                HttpResponse response = httpclient.execute(httpget);
                str = EntityUtils.toString(response.getEntity());

            } catch (IOException e) {
                e.printStackTrace();
            }// catch (JSONException e) {
            //    e.printStackTrace();
            //}
            Log.i("---->>> EthnicGroups-->", str);
            return str;
        }
    }


}
