package com.inkpon.app;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.EditText;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

/**
 * Created by hounge on 9/25/2014.
 */
public class ForgotPassword extends Activity {
    public static final String Stored_Token = "TokenFile";
    EditText forgot_email;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.requestpass);

        forgot_email = (EditText) findViewById(R.id.editTextPassword);
    }

        public void onForgot(View view){
        String femail = forgot_email.getText().toString();
        SharedPreferences prefs = getSharedPreferences(Stored_Token,MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences(Stored_Token, MODE_PRIVATE).edit();
        editor.putString("Email", femail);
        editor.apply();

        try{
            String email = prefs.getString("Email", null);
            JSONObject passrequest = new JSONObject();
            passrequest.put("Email", email);
            Resources resources=getResources();
            final String urla=String.format(resources.getString(R.string.forgot_password));
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(urla);
            httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
            httpPost.setEntity(new StringEntity(passrequest.toString(), HTTP.UTF_8));
            //gets Response from URL
            HttpResponse responseGet = client.execute(httpPost);
    } catch (Exception e) {

        e.printStackTrace();
    }
        Intent i = new Intent(ForgotPassword.this, Login.class);
        startActivity(i);

    }



}
