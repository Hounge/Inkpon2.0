package com.inkpon.app;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapters.BaseAdapter3;

/**
 * Created by hounge on 9/25/2014.
 */
public class AccountDetails extends Activity {
    public static final String Stored_Token = "TokenFile";
    ArrayList<String> title_array = new ArrayList<String>();
    ArrayList<String> notice_array = new ArrayList<String>();
    ListView list;
    TextView balance1;
    BaseAdapter3 adapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accountdetails);
        SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        list = (ListView) findViewById(R.id.detailslist);
        balance1 = (TextView) findViewById(R.id.textViewPassword);
        String balance = prefs.getString("CurrentBalance", null);
        balance1.setText(balance);

        new TheTask().execute();
    }


    public void BackButton(View view){
        this.finish();
    }
    public void onRefresh(View view){
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    class TheTask extends AsyncTask<Void, Void, String> {
        public static final String Stored_Token = "TokenFile";
        SharedPreferences prefs = getSharedPreferences(Stored_Token, MODE_PRIVATE);
        String Token = prefs.getString("Token", null);
        String APICALL = prefs.getString("APICall", null);


        @Override
        protected String doInBackground(Void... params) {
            String str = null;
            try {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                JSONObject Details = new JSONObject();
                Details.put("Token", Token);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(APICALL);
                httppost.setHeader(HTTP.CONTENT_TYPE, "application/json; charset=utf-8");
                httppost.setEntity(new StringEntity(Details.toString(), HTTP.UTF_8));
                HttpResponse response = httpclient.execute(httppost);

                /*--check http status code...if token is dropped/erased, basically it is no longer valid!*/
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_UNAUTHORIZED){
                    Log.i("***Token: ",response.toString()+" no longer valid...");
                    str="";
                    //Toast.makeText(getApplicationContext(),response.toString(),Toast.LENGTH_LONG).show();
                }else {
                    str = EntityUtils.toString(response.getEntity());
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                //Log.i("**Error-1**",e.toString());
                e.printStackTrace();
            } catch (JSONException e) {
                //Log.i("**Error-2**",e.toString());
                e.printStackTrace();
            }

            Log.i("response", str);
            return str;

        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            String response = result.toString();
            try {
                JSONArray new_array = new JSONArray(response);
                for (int i = 0, count = new_array.length(); i < count; i++) {
                    try {
                        JSONObject jsonObject = new_array.getJSONObject(i);
                        title_array.add(jsonObject.getString("Date").toString());
                        notice_array.add(jsonObject.getString("Total").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                adapter = new BaseAdapter3(AccountDetails.this, title_array, notice_array);
                list.setAdapter(adapter);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                // tv.setText("error2");
            }

        }


    }

}
