package adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.inkpon.app.R;

import java.util.ArrayList;

/**
 * Created by hounge on 10/9/14.
 */
public class ChatAdapter extends BaseAdapter {

    private Activity activity;

    private static ArrayList title;
    private static LayoutInflater inflater = null;

    public ChatAdapter(Activity a, ArrayList b) {
        activity = a;
        this.title = b;


        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return title.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.chatlist, null);

        TextView title2 = (TextView) vi.findViewById(R.id.Hours); // title
        String song = title.get(position).toString();
        title2.setText(song);




        return vi;

    }
}
